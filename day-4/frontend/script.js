const DOMAIN = "http://localhost:3000";

function getAndDisplayAllHeroes() {
    $.ajax({
        method: "GET",
        url: `${DOMAIN}/heroes`
    }).done((resp) => {
        const heroContainerEl = document.getElementById("hero-container"); 
        heroContainerEl.innerHTML = "";

        resp.forEach(hero => {
            const newDivEl = document.createElement('div');
            newDivEl.textContent = `${hero.id}: ${hero.name} - ${hero.age}`

            heroContainerEl.appendChild(newDivEl);
        });  
    })
}

$(document).ready(function() {
    //alert("it works");
    $("#add").click(function(){
         // var descVal = $('#description').val()
         var nameVal = $("input[name=name]").val();
         var ageVal = $("input[name=age]").val();
         var image_urlVal = $("input[name=image_url]").val();
         var heroObj = {name: nameVal, age: ageVal, image_url: image_urlVal};
         //var jsonString = JSON.stringify(heroObj);
         $.ajax({
             url: `${DOMAIN}/heroes`,
             type: 'POST',
             dataType: "json",
             data: heroObj,
             error: function(jqxhr, status, errorThrown) {
                 httpErrorHandler(jqxhr, status, errorThrown);
             }
         }).done(function(peopleList){
             alert("Record added");
         });
    });

 });

 function httpErrorHandler(jqxhr, status, errorThrown) {
         alert("AJAX error: " + jqxhr.responseText + ", status: " + jqxhr.status);
 }