'use strict';

var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'rootRoot1',
  database : 'user_management'
});
 
connection.connect(function(err){
    if (err) throw err;
    console.log("DB connection successful");
});

module.exports = connection;