// Print something
console.log("Hey! Writting node.js")

// Conditions
let age = 20;

//if condition
if (age > 18){
    console.log("You can drink");
}
else{
    console.log("You cannot drink");
}

// if else-if
if (age > 18){
    console.log("You can drink");
}
else if(age > 12){
    console.log("You can have juice");
}
else{
    console.log("You can only have juice");
}

//Loops

for(let i=0; i<10; i++){
    console.log(i);
}

let i=0;
while(i<10){
    console.log(i);
    i++;
}

let j=0;
do {
    console.log(j);
    j++;
}
while(j < 10)

// =================Object
const student = {
    name: "flash",
    studentid: "1234",
    courses: ["JS","Java", "PHP", ".NET"],
    program: {
        name: "IPD",
        duration: "12 month",
        noOfCourses: 10,
        campus: {
            name: "john abbott"
        }
    },
    sayHi: function(){
        console.log("Hey")
    }
}

student.name;
student.studentid;
student["studentid"];
student.sayHi();

//==========Functions

function adder(x,y){
    const sum = x+y;
    return sum;
}
adder(5,10);

const adder2 = function(x,y){
    const sum = x+y;
    return sum;
}
adder2(5,10);
// using fat arrow operator
const adder3 = (x,y) => {
    const sum = x+y;
    return sum;
}
adder3(5,10);

const numbers = [1,2,3,4,5,6,7,8,9];
numbers.forEach(function(number){
    console.log(number*3);
})

numbers.forEach(number => console.log(number*3))

const numbers2 = [5,7,45,435,656,2,546,45,784,243,6,87];
numbers2.find(n => n>50)
numbers2.push(1234);

numbers2.forEach(n => {
    if(n > 50){
        console.log(n)
    }
})
const students = [
    {
        name: "superwoman",
        marks: 90
    },
    {
        name: "flash",
        marks: 70
    },
    {
        name: "batman",
        marks: 77
    },
    {
        name: "superman",
        marks: 60
    },
    {
        name: "arrow",
        marks: 94
    }
]

students.sort((a,b) => {
  if (a.marks > b.marks){
    return 1;
  }else if (a.marks < b.marks){
    return -1;
}});
students.reverse();
students.sort((a,b) =>(a.marks > b.marks)? 1:-1);
students.reverse();
//sorting desc
const studentSort = students.sort((a, b) => {
    return b.marks - a.marks;
  });

  console.log(studentSort);
const studentFilter = students.filter(s => s.marks > 80);
console.log(studentFilter);
const studentMap = students.map(s => s.marks + 5);
console.log(studentMap);


