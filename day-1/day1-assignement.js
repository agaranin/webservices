const students = [
    {
        name: "superwoman",
        marks: 90
    },
    {
        name: "flash",
        marks: 70
    },
    {
        name: "batman",
        marks: 77
    },
    {
        name: "superman",
        marks: 60
    },
    {
        name: "arrow",
        marks: 94
    }
]

// Sort version 1
students.sort((a,b) => {
  if (a.marks > b.marks){
    return -1;
  }else if (a.marks < b.marks){
    return 1;
}});
console.log("Sort V1:", students);

// Sort version 2
students.sort((a,b) =>(a.marks > b.marks)? -1:1);
console.log("Sort V2:", students);

// Sort version 3
students.sort((a, b) => {return b.marks - a.marks;});
//students.reverse();
console.log("Sort V3:", students);

// Filter
studentsFilter = students.filter(s => s.marks > 80);
console.log("Filer:", studentsFilter);

// Map. Change marks and return all objects
const studentMap = students.map(s => {s.marks += 5; return s});
console.log("Map V1:", studentMap);

// Map. Return only chenged marks
const studentMap2 = students.map(s => s.marks + 5);
console.log("Map V2:", studentMap2);