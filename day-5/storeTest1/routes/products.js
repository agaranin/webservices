const express = require('express');
const router = express.Router();
const sql = require('../db');
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});


const qurySchema = Joi.object({
  name: Joi.string()
    .min(3)
    .max(10)
    .required()
})
//- GET API to get the list of all products in inventory
router.get('/', validator.query(qurySchema), function(req, res, next) {
  let sqlString = "select id, sku_id, product_name, expiry_date, DATEDIFF(expiry_date, CURDATE()) AS 'days_to_expire_from_today' from products";
  if(req.query && req.query.name){
    sqlString += ` where product_name = '${req.query.name}'`
  }
  sql.query(sqlString, function(error, results, fields){
    if(error) throw error;
    const options = {day: 'numeric', month: 'long', year: 'numeric' };
    output = results.map(r => {
      let d = r.expiry_date;
      d = d.toLocaleDateString(undefined, {day:'2-digit'}) + ' ' + d.toLocaleDateString(undefined, {month:'long'}) + ', ' + d.toLocaleDateString(undefined, {year:'numeric'});
      r.expiry_date = d;
      return r;
    });
    res.json(output);
  })
});

//Validation the body params of the post api using express-joi-validation package
const bodySchema = Joi.object({
  sku_id: Joi.string()
    .required(),
  product_name: Joi.string()
    .required(),
  expiry_date: Joi.date()
    .required()
})

//POST API to create a new product
router.post('/', validator.body(bodySchema), function(req, res, next) {
  console.log(req.body);
  const data = req.body
  sql.query('INSERT INTO products SET ?', data, function(error, results, fields){
    if(error) {
      console.log("error: ", error);
      res.send(error);
    }
    else{
    res.json({"Created id":results.insertId});
    }
  })
});

//GET API to get one product by id
router.get('/:id([0-9]+)', function(req, res, next) {
  sql.query("select id, sku_id, product_name, expiry_date, DATEDIFF(expiry_date, CURDATE()) AS 'days_to_expire_from_today' from products where id = ?",[req.params.id], function(error, results, fields){
    if(error) throw error;
    const options = {day: 'numeric', month: 'long', year: 'numeric' };
    output = results.map(r => {
      let d = r.expiry_date;
      d = d.toLocaleDateString(undefined, {day:'2-digit'}) + ' ' + d.toLocaleDateString(undefined, {month:'long'}) + ', ' + d.toLocaleDateString(undefined, {year:'numeric'});
      r.expiry_date = d;
      return r;
    });
    res.json(output);
  })
});

//GET API to get one product by sku_id
router.get('/:sku_id([a-z]{2}[0-9]+)', function(req, res, next) {
  sql.query("select id, sku_id, product_name, expiry_date, DATEDIFF(expiry_date, CURDATE()) AS 'days_to_expire_from_today' from products where sku_id = ?",[req.params.sku_id], function(error, results, fields){
    if(error) throw error;
    const options = {day: 'numeric', month: 'long', year: 'numeric' };
    output = results.map(r => {
      let d = r.expiry_date;
      d = d.toLocaleDateString(undefined, {day:'2-digit'}) + ' ' + d.toLocaleDateString(undefined, {month:'long'}) + ', ' + d.toLocaleDateString(undefined, {year:'numeric'});
      r.expiry_date = d;
      return r;
    });
    res.json(output);
  })
});

//PUT API to update one product by id
router.put('/:id([0-9]+)', function(req, res, next) {
  const id = req.params.id;
  const data = req.body;
  sql.query("UPDATE products SET sku_id = ?, product_name = ?, expiry_date = ?  where id = ?",[data.sku_id, data.product_name, data.expiry_date, id], function(error, results, fields){
    if(error) {
      console.log("error: ", error);
      res.send(error);
    }
    else{
      res.send({"Rows changed": results.changedRows});
    }
    });
});
module.exports = router;
