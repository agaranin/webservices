CREATE DATABASE storedb;
USE storedb;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `sku_id` varchar(200) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `expiry_date` datetime NOT NULL
);
 
ALTER TABLE `products` ADD PRIMARY KEY (`id`);
ALTER TABLE `products` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `products` (`id`, `sku_id`, `product_name`, `expiry_date`) VALUES
(1, 'iy4169', 'Butter', '2021-03-29'),
(2, 'ek613', 'Whole grain bread', '2021-04-15'),
(3, 'pb016', 'Almond Milk', '2021-05-05');

