var express = require('express');
var router = express.Router();
const sql = require('../db');

// - get api to get one order by id
router.get('/:id([0-9]+)', function(req, res, next) {
  const id = req.params.id;
  sql.query("Select * from superheros where id = ? ", id, function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send(result);
    }
    });
});

// - put api to edit one order by id
router.put('/:id', function(req, res, next) {
  const id = req.params.id;
  const data = req.body[0];
  var CURRENT_TIMESTAMP = { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
  sql.query('UPDATE superheros SET name = ?, age = ?, image_url = ?, created_at = ? where id = ?', [data.name, data.age, data.image_url, CURRENT_TIMESTAMP, id], function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send({"Rows affected": result.changedRows});
    }
    });
});

// - delete api to delete one record by id
router.delete('/:id', function(req, res, next) {
  const id = req.params.id;
  sql.query('DELETE FROM superheros WHERE id = ?', [id], function(error, results, fields){
    if(error) {
      console.log("error: ", error);
      res.send(error);
    }
    else{
    res.json({"affected rows":results.affectedRows});
    }
  })
});

// - post api to create a new record
router.post('/', function(req, res, next) {
  console.log(req.body);
  const data = req.body
  sql.query('INSERT INTO superheros SET ?', data, function(error, results, fields){
    if(error) {
      console.log("error: ", error);
      res.send(error);
    }
    else{
    res.json({"Created id":results.insertId});
    }
  })
});

// - patch api to update part of one record by id (Name)
router.patch('/:id', function(req, res, next) {
  const id = req.params.id;
  const data = req.body[0];
  //var CURRENT_TIMESTAMP = { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
  sql.query('UPDATE superheros SET name = ? where id = ?', [data.name, id], function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send({"Rows affected": result.changedRows});
    }
    });
});

// All resources
//- get api to fetch all the records
router.get('/', function(req, res, next) {
  sql.query("select * from superheros", function(error, results, fields){
    if(error) throw error;
    newres = results.map(r => {r.created_at = r.created_at.toLocaleDateString(); return r});
    res.json(newres);
  })
});

// - get api to fetch all names and ages of the records
router.get('/names', function(req, res, next) {
  sql.query("select name from superheros", function(error, results, fields){
    if(error) throw error;
    console.log(results);
    res.json(results);
  })
});

// - get api to fetch all names and ages of the records
router.get('/ages', function(req, res, next) {
  sql.query("select age from superheros", function(error, results, fields){
    if(error) throw error;
    console.log(results);
    res.json(results);
  })
});

// - put api to edit all the records
router.put('/', function(req,res){
  const dataSet = req.body;
  let affectedRows = 0;
  const CURRENT_TIMESTAMP = { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
  dataSet.forEach(data => {
    sql.query('UPDATE superheros SET name = ?, age = ?, image_url = ?, created_at = ? where id = ?', [data.name, data.age, data.image_url, CURRENT_TIMESTAMP, data.id], function (err, result) {
      if(err) {
        console.log("error: ", err);
      }
      });
  });
  res.json({"resp": "Records successfully updated" })
})

// - delete api to delete all orders 
router.delete('/', function(req,res){
  sql.query('TRUNCATE TABLE superheros', function(error, results, fields){
    if(error) {
      console.log("error: ", error);
      res.send(error);
    }
    else{
    res.json(results);
    }
  })
})

module.exports = router;
