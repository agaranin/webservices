-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema librarydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema librarydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `librarydb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `librarydb` ;

-- -----------------------------------------------------
-- Table `librarydb`.`authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `librarydb`.`authors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(200) NOT NULL,
  `last_name` VARCHAR(200) NOT NULL,
  `gender` ENUM('male', 'female', 'other') NOT NULL,
  `nationality` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `librarydb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `librarydb`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(200) NOT NULL,
  `last_name` VARCHAR(200) NOT NULL,
  `gender` ENUM('male', 'female', 'other') NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `librarydb`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `librarydb`.`books` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NOT NULL,
  `description` VARCHAR(2000) NOT NULL,
  `year` YEAR NOT NULL,
  `author_id` INT NOT NULL,
  `publisher` VARCHAR(200) NOT NULL,
  `isbn` VARCHAR(45) NOT NULL,
  `user_id` INT NULL DEFAULT NULL,
  `is_available` TINYINT NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  INDEX `books_authors_fk_idx` (`author_id` ASC) VISIBLE,
  INDEX `books_users_fk_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `books_authors_fk`
    FOREIGN KEY (`author_id`)
    REFERENCES `librarydb`.`authors` (`id`),
  CONSTRAINT `books_users_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `librarydb`.`users` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `librarydb`.`users_books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `librarydb`.`users_books` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `book_id` INT NOT NULL,
  `borrowed_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id_book_id_unique` (`user_id` ASC, `book_id` ASC) VISIBLE,
  INDEX `users_books_book_id_fk_idx` (`book_id` ASC) VISIBLE,
  CONSTRAINT `users_books_book_id_fk`
    FOREIGN KEY (`book_id`)
    REFERENCES `librarydb`.`books` (`id`),
  CONSTRAINT `users_books_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `librarydb`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
