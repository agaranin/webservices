const DOMAIN = "https://library-express-js-project.herokuapp.com";
var currUserId = 0;
var currBookId = 0;
var currAuthorId = 0;
var loggedUserId = 1;

function httpErrorHandler(jqxhr, status, errorThrown) {
  alert("AJAX error: " + jqxhr.responseText + ", status: " + jqxhr.status);
};

$(document).ready(function () {
  getUsers();
});
// user functions

function DeleteUserById(id) {
  $.ajax({
    url: `${DOMAIN}/users/${id}`,
    method: "DELETE",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (user) {
    alert("Record deleted successfully");
    getUsers();
  });
};

function showUserForm() {
  $("#user-table").hide();
  $("#user-form").show();
  $('#user-total').empty();
  var result =
    '<h4>Add a new user</h4>' +
    '<div class="form-group">' +
    '<label for="first_name">First Name</label>' +
    '<input type="text" class="form-control" name="first_name">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="last_name">Last Name</label>' +
    '<input type="text" class="form-control" name="last_name">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="gender">Gender</label>' +
    '<input type="text" class="form-control" name="gender">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="email">Email</label>' +
    '<input type="text" class="form-control" name="email">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="password">Password</label>' +
    '<input type="text" class="form-control" name="password">' +
    "</div>" +
    '<div class=""><br>' +
    '<button class=" btn btn-success" id="btnAddEdit" onclick="addEditUser()">Update</button>' +
    "</div>";
  $("#user-form").html(result);
};

function EditUserById(id) {
  $.ajax({
    url: `${DOMAIN}/users/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (user) {
    currUserId = user[0].id;
    showUserForm();
    $('h4').text("Edit the user");
    $("input[name=first_name]").val(user[0].first_name);
    $("input[name=last_name]").val(user[0].last_name);
    $("input[name=gender]").val(user[0].gender);
    $("input[name=email]").val(user[0].email);
    $("input[name=password]").val(user[0].password);
  });
};

function UserDetailsById(id) {
  $.ajax({
    url: `${DOMAIN}/users/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (user) {
    $("#user-table").hide();
    $('#user-total').empty();
    const u = user[0];
    const newUserEl = document.createElement("div");
    newUserEl.className = "col";
    newUserEl.innerHTML =
            `
            <h4>User</h4>
            <hr />
            <dl class="dl-horizontal">
                <dt>
                    First Name
                </dt>
        
                <dd>
                    ${u.first_name}
                </dd>
        
                <dt>
                    Last name
                </dt>
        
                <dd>
                    ${u.last_name}
                </dd>
        
                <dt>
                    Gender
                </dt>
        
                <dd>
                    ${u.gender}
                </dd>
                <dt>
                    Email
                </dt>
        
                <dd>
                    ${u.email}
                </dd>
                <dt>
                    Password
                </dt>
        
                <dd>
                    ${u.password}
                </dd>
        
            </dl>

        `
        $('#user-details').append(newUserEl);
    currUserId = user[0].id;
   })
   .always(function () {
    UserBooksById(currUserId);
  });
};

  function UserBooksById(id) {
  $.ajax({
    url: `${DOMAIN}/books/user/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (books) {
    const newBooksEl = document.createElement("div");
    newBooksEl.className = "col";
    newBooksEl.id = "user-books";
    var result =
      "<h4>Borrowed Books</h4><hr /><table class='table table-striped table-hover table-bordered'>" + "<tr><th>ID</th>" + "<th>Title</th>" + "<th>Description</th>"+ "<th>Author</th>"+ "<th></th>";
    books.forEach((book) => {
      result += "<tr><td>" + book.id + "</td>";
      result += "<td>" + book.title + "</td>";
      result += "<td>" + book.description + "</td>";
      result += "<td>" + book.author + "</td>";
      result += "<td><a onclick='return returnBookById("+ book.id +");' href='#'>Return</a></td>";
    });
    result += "</tr>" + "\n";
    newBooksEl.innerHTML = result;
    $('#user-details').append(newBooksEl);
  });
};

function returnBookById(id) {
  $.ajax({
    url: `${DOMAIN}/books/${id}`,
    method: "PATCH",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (user) {
      alert("Book has been returned successfully");
      $('#user-details').empty();
    })
    .always(function () {
      UserDetailsById(currUserId);
    });
};

function addNewUser() {
  showUserForm();
  $("#btnAddEdit").html("Add");
};

function addEditUser() {
  var firstNameVal = $("input[name=first_name]").val();
  var lastNameVal = $("input[name=last_name]").val();
  var genderVal = $("input[name=gender]").val();
  var emailVal = $("input[name=email]").val();
  var passwordVal = $("input[name=password]").val();
  var userObj = {
    first_name: firstNameVal,
    last_name: lastNameVal,
    gender: genderVal,
    email: emailVal,
    password: passwordVal,
  };
  $.ajax({
    url: `${DOMAIN}/users` + (currUserId == 0 ? "" : "/" + currUserId),
    method: currUserId == 0 ? "POST" : "PUT",
    data: userObj, // body of the request
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (user) {
      alert("Record " + (currUserId == 0 ? "added" : "updated") + " successfully");
      currUserId = 0;
    })
    .always(function () {
      getUsers();
    });
};


function getUsers() {
  $.ajax({
    method: "GET",
    url: `${DOMAIN}/users`,
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done((resp) => {
    $("#user-container").show();
    $("#author-container").hide();
    $("#book-container").hide();
    $("#user-table").show();
    $('#user-details').empty();
    $('#user-total').empty();
    $("#user-form").hide();
    currUserId = 0;
    var result =
      '<caption style="caption-side:top"><a onclick="return addNewUser();" href="#">Add new user</a></br></br></caption>' +
      "<tr><th>ID</th>" +
      "<th>First Name</th>" +
      "<th>Last Name</th>" +
      "<th>Gender</th>" +
      "<th>Email</th>" +
      "<th>Password</th><th>Action</th></tr>";
    resp.forEach((user) => {
      // result += '<tr onclick="selectItem(' + todo.id + ')">';
      result += "<tr><td>" + user.id + "</td>";
      result += "<td>" + user.first_name + "</td>";
      result += "<td>" + user.last_name + "</td>";
      result += "<td>" + user.gender + "</td>";
      result += "<td>" + user.email + "</td>";
      result += "<td>" + user.password + "</td>";
      result +=
        '<td><a onclick="return EditUserById(' +
        user.id +
        ');" href="#">Edit</a>' +
        ' <a onclick="return DeleteUserById(' +
        user.id +
        ');" href="#">Delete</a>' +
        ' <a onclick="return UserDetailsById(' +
        user.id +
        ');" href="#">Details</a></td>';
      result += "</tr>" + "\n";
      $("#user-table").html(result);
    });
  })
  .always(function () {
    CountUsers();
  });
};

function CountUsers() {
  $.ajax({
    url: `${DOMAIN}/users/count`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (result) {
      $('#user-total').append(`<p>User total: ${result[0].count}</p>`);
    });
};






// books functions

function getBooks() {
  $.ajax({
    method: "GET",
    url: `${DOMAIN}/books`,
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done((resp) => {
    $("#book-container").show();
    $("#user-container").hide();
    $("#author-container").hide();
    $('#book-details').empty();
    $('#book-total').empty();
    $("#book-form").hide();
    $("#book-table").show();
    currBookId = 0;
    var result =
      '<caption style="caption-side:top"><a onclick="return addNewBook();" href="#">Add a new book</a></br></br></caption>' +
      "<tr><th>ID</th>" +
      "<th>Title</th>" +
      "<th>Description</th>" +
      "<th>Borrow</th>" +
      "<th>Action</th></tr>";
    resp.forEach((book) => {
      // result += '<tr onclick="selectItem(' + todo.id + ')">';
      result += "<tr><td>" + book.id + "</td>";
      result += "<td>" + book.title + "</td>";
      result += "<td>" + book.description + "</td>";
      if(book.user_id != null)
      result +='<td><u>Unavailable</u></a>';
      else
      result +='<td><a onclick="return borrowBookById('+book.id +');" href="#">Borrow</a>';
      result +='<td><a onclick="return EditBookById(' +
      book.id +
        ');" href="#">Edit</a>' +
        ' <a onclick="return DeleteBookById(' +
        book.id +
        ');" href="#">Delete</a>' +
        ' <a onclick="return BookDetailsById(' +
        book.id +
        ');" href="#">Details</a></td>';
      result += "</tr>" + "\n";
      $("#book-table").html(result);
    });
  }).always(function () {
    CountBooks();
  });
};

function CountBooks() {
  $.ajax({
    url: `${DOMAIN}/books/count`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (result) {
      $('#book-total').append(`<p>Book total: ${result[0].count}</p>`);
    });
};

function showBookForm() {
  $("#book-table").hide();
  $("#book-form").show();
  $('#book-total').empty();
  var result =
    '<h4>Add a new book</h4>' +
    '<div class="form-group">' +
    '<label for="title">Title</label>' +
    '<input type="text" class="form-control" name="title">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="description">Description</label>' +
    '<input type="text" class="form-control" name="description">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="year">Year</label>' +
    '<input type="text" class="form-control" name="year">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="author_id">Author</label>' +
    '<input type="text" class="form-control" name="author_id">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="publisher">Publisher</label>' +
    '<input type="text" class="form-control" name="publisher">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="isbn">ISBN</label>' +
    '<input type="text" class="form-control" name="isbn">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="user_id">User</label>' +
    '<input type="text" class="form-control" name="user_id">' +
    "</div>" +
    '<div class=""><br>' +
    '<button class=" btn btn-success" id="btnAddEditBook" onclick="addEditBook()">Update</button>' +
    "</div>";
  $("#book-form").html(result);
};

function addNewBook() {
  showBookForm();
  $("#btnAddEditBook").html("Add");
};

function addEditBook() {
  var titleVal = $("input[name=title]").val();
  var descriptionVal = $("input[name=description]").val();
  var yearVal = $("input[name=year]").val();
  var authorIdVal = $("input[name=author_id]").val();
  var publisherVal = $("input[name=publisher]").val();
  var isbnVal = $("input[name=isbn]").val();
  var userIdVal = $("input[name=user_id]").val();
  var bookObj = {
    title: titleVal,
    description: descriptionVal,
    year: yearVal,
    author_id: authorIdVal,
    publisher: publisherVal,
    isbn: isbnVal,
    user_id: userIdVal,
    is_available: true
  };
  $.ajax({
    url: `${DOMAIN}/books` + (currBookId == 0 ? "" : "/" + currBookId),
    method: currBookId == 0 ? "POST" : "PUT",
    data: bookObj, // body of the request
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (user) {
      alert("Record " + (currBookId == 0 ? "added" : "updated") + " successfully");
      currBookId = 0;
    })
    .always(function () {
      getBooks();
    });
};

function DeleteBookById(id) {
  $.ajax({
    url: `${DOMAIN}/books/${id}`,
    method: "DELETE",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (book) {
    alert("Record deleted successfully");
    getBooks();
  });
};

function EditBookById(id) {
  $.ajax({
    url: `${DOMAIN}/books/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (book) {
    currBookId = book[0].id;
    showBookForm();
    $('h4').text("Edit the book");
    $("input[name=title]").val(book[0].title);
    $("input[name=description]").val(book[0].description);
    $("input[name=year]").val(book[0].year);
    $("input[name=author_id]").val(book[0].author_id);
    $("input[name=publisher]").val(book[0].publisher);
    $("input[name=isbn]").val(book[0].isbn);
    $("input[name=user_id]").val(book[0].user_id);
  });
};

function BookDetailsById(id) {
  $.ajax({
    url: `${DOMAIN}/books/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (book) {
    $("#book-table").hide();
    $('#book-total').empty();
    const b = book[0];
    const newBookEl = document.createElement("div");
    newBookEl.className = "col";
    newBookEl.innerHTML =
            `
            <h4>Book</h4>
            <hr />
            <dl class="dl-horizontal">
                <dt>
                    Title
                </dt>
        
                <dd>
                    ${b.title}
                </dd>
        
                <dt>
                    Description
                </dt>
        
                <dd>
                    ${b.description}
                </dd>
        
                <dt>
                    Year
                </dt>
        
                <dd>
                    ${b.year}
                </dd>
                <dt>
                    Author
                </dt>
        
                <dd>
                    ${b.author_id}
                </dd>
                <dt>
                    Publisher
                </dt>
        
                <dd>
                    ${b.publisher}
                </dd>
                <dt>
                    ISBN
                </dt>
        
                <dd>
                    ${b.isbn}
                </dd>
                <dt>
                    Available
                </dt>
        
                <dd>
                    ${b.is_available}
                </dd>
        
            </dl>

        `
        $('#book-details').append(newBookEl);
    currBookId = book[0].id;
   })
};

function borrowBookById(id) {
  $.ajax({
    url: `${DOMAIN}/books/borrow/${id}`,
    method: "PATCH",
    data: {user_id: loggedUserId},
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (user) {
      alert("Book has been borrowed successfully");
    })
    .always(function () {
      getBooks();
    });
};

// Authors functions

function getAuthors() {
  $.ajax({
    method: "GET",
    url: `${DOMAIN}/authors`,
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done((resp) => {
    $("#book-container").hide();
    $("#user-container").hide();
    $("#author-container").show();
    $('#author-details').empty();
    $('#author-total').empty();
    $("#author-form").hide();
    $("#author-table").show();
    currAuthorId = 0;
    var result =
      '<caption style="caption-side:top"><a onclick="return addNewAuthor();" href="#">Add new author</a></br></br></caption>' +
      "<tr><th>ID</th>" +
      "<th>First Name</th>" +
      "<th>Last Name</th>" +
      "<th>Gender</th>" +
      "<th>Nationality</th><th>Action</th></tr>";
    resp.forEach((author) => {
      result += "<tr><td>" + author.id + "</td>";
      result += "<td>" + author.first_name + "</td>";
      result += "<td>" + author.last_name + "</td>";
      result += "<td>" + author.gender + "</td>";
      result += "<td>" + author.nationality + "</td>";
      result +='<td><a onclick="return EditAuthorById(' +
      author.id +
        ');" href="#">Edit</a>' +
        ' <a onclick="return DeleteAuthorById(' +
        author.id +
        ');" href="#">Delete</a>' +
        ' <a onclick="return AuthorDetailsById(' +
        author.id +
        ');" href="#">Details</a></td>';
      result += "</tr>" + "\n";
      $("#author-table").html(result);
    });
  }).always(function () {
    CountAuthors();
  });
};

function CountAuthors() {
  $.ajax({
    url: `${DOMAIN}/authors/count`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (result) {
      $('#author-total').append(`<p>Author total: ${result[0].count}</p>`);
    });
};

function showAuthorForm() {
  $("#author-table").hide();
  $("#author-form").show();
  $('#author-total').empty();
  var result =
    '<h4>Add a new author</h4>' +
    '<div class="form-group">' +
    '<label for="first_name">First Name</label>' +
    '<input type="text" class="form-control" name="first_name">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="last_name">Last Name</label>' +
    '<input type="text" class="form-control" name="last_name">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="gender">Gender</label>' +
    '<input type="text" class="form-control" name="gender">' +
    "</div>" +
    '<div class="form-group">' +
    '<label for="nationality">Nationality</label>' +
    '<input type="text" class="form-control" name="nationality">' +
    "</div>" +
    '<div class=""><br>' +
    '<button class=" btn btn-success" id="btnAddEditAuthor" onclick="addEditAuthor()">Update</button>' +
    "</div>";
  $("#author-form").html(result);
};

function addNewAuthor() {
  showAuthorForm();
  $("#btnAddEditAuthor").html("Add");
};

function addEditAuthor() {
  var firstNameVal = $("input[name=first_name]").val();
  var lastNameVal = $("input[name=last_name]").val();
  var genderVal = $("input[name=gender]").val();
  var nationalityVal = $("input[name=nationality]").val();
  var authorObj = {
    first_name: firstNameVal,
    last_name: lastNameVal,
    gender: genderVal,
    nationality: nationalityVal,
  };
  $.ajax({
    url: `${DOMAIN}/authors` + (currAuthorId == 0 ? "" : "/" + currAuthorId),
    method: currAuthorId == 0 ? "POST" : "PUT",
    data: authorObj, // body of the request
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  })
    .done(function (author) {
      alert("Record " + (currBookId == 0 ? "added" : "updated") + " successfully");
      currAuthorId = 0;
    })
    .always(function () {
      getAuthors();
    });
};

function DeleteAuthorById(id) {
  $.ajax({
    url: `${DOMAIN}/authors/${id}`,
    method: "DELETE",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (author) {
    alert("Record deleted successfully");
    getAuthors();
  });
};

function EditAuthorById(id) {
  $.ajax({
    url: `${DOMAIN}/authors/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (author) {
    currAuthorId = author[0].id;
    showAuthorForm();
    $('h4').text("Edit the author");
    $("input[name=first_name]").val(author[0].first_name);
    $("input[name=last_name]").val(author[0].last_name);
    $("input[name=gender]").val(author[0].gender);
    $("input[name=nationality]").val(author[0].nationality);
  });
};

function AuthorDetailsById(id) {
  $.ajax({
    url: `${DOMAIN}/authors/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (author) {
    $("#author-table").hide();
    $('#author-total').empty();
    const a = author[0];
    const newAuthorEl = document.createElement("div");
    newAuthorEl.className = "col";
    newAuthorEl.innerHTML =
            `
            <h4>Author</h4>
            <hr />
            <dl class="dl-horizontal">
                <dt>
                    First Name
                </dt>
        
                <dd>
                    ${a.first_name}
                </dd>
        
                <dt>
                    Last Name
                </dt>
        
                <dd>
                    ${a.last_name}
                </dd>
        
                <dt>
                    Gender
                </dt>
        
                <dd>
                    ${a.gender}
                </dd>
                <dt>
                    Nationality
                </dt>
        
                <dd>
                    ${a.nationality}
                </dd>
            </dl>

        `
        $('#author-details').append(newAuthorEl);
    currAuthorId = author[0].id;
   })
   .always(function () {
    AuthorBooksById(currAuthorId);
  });
};

function AuthorBooksById(id) {
  $.ajax({
    url: `${DOMAIN}/books/author/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (books) {
    const newBooksEl = document.createElement("div");
    newBooksEl.className = "col";
    newBooksEl.id = "author-books";
    var result =
      "<h4>Author Books</h4><hr /><table class='table table-striped table-hover table-bordered'>" + "<tr><th>ID</th>" + "<th>Title</th>" + "<th>Description</th>"+ "<th>Author</th>";
    books.forEach((book) => {
      result += "<tr><td>" + book.id + "</td>";
      result += "<td>" + book.title + "</td>";
      result += "<td>" + book.description + "</td>";
      result += "<td>" + book.author + "</td>";
    });
    result += "</tr>" + "\n";
    newBooksEl.innerHTML = result;
    $('#author-details').append(newBooksEl);
  });
};