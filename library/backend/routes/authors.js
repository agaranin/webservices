var express = require('express');
var router = express.Router();
const sql = require('../db');
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({})

/* GET authors listing. */
router.get('/', function(req, res, next) {
  sql.query("select * from authors",function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});

/* GET authors count. */
router.get('/count', function(req, res, next) {
  sql.query("select COUNT(id) as 'count' from authors",function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});

// - get api to get one author by id
router.get('/:id([0-9]+)', function(req, res, next) {
  const id = req.params.id;
  sql.query("Select * from authors where id = ? ", id, function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send(result);
    }
    });
});


const createAuthorSchema = Joi.object({
  first_name: Joi.string().required(),
  last_name: Joi.string().required(),
  gender: Joi.string().required(),
  nationality: Joi.string().required()
});

// POST /authors
router.post("/", validator.body(createAuthorSchema), (req, res, next) => {
  sql.query("INSERT into authors SET ?", req.body, (error, result) => {
    if (error) {
      next(error);
      return;
    }
    res.json(result);
  });
});

//Delete author by id
router.delete('/:id',function(req,res){
  //finding Index
  sql.query('DELETE FROM authors WHERE id=?', [req.params.id], function(error, results, fields) {
    if (error) throw error;
    res.end('Record has been deleted!');
    });
});

//edit author by id 
router.put('/:id',function(req,res){
  sql.query('UPDATE authors SET first_name=?, last_name=?, gender=?, nationality=? where id=?', [req.body.first_name, req.body.last_name, req.body.gender, req.body.nationality, req.params.id], function(error, results, fields) {
  if (error) throw error;
     res.json(results);
});
});

module.exports = router;
