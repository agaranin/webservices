var express = require("express");
var router = express.Router();
const sql = require("../db");
const Joi = require("joi");
const validator = require("express-joi-validation").createValidator({});

/* GET users listing. */
router.get('/', function(req, res, next) {
  sql.query("select * from users",function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});

/* GET users count. */
router.get('/count', function(req, res, next) {
  sql.query("select COUNT(id) as 'count' from users",function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});

// - get api to get one user by id
router.get('/:id([0-9]+)', function(req, res, next) {
  const id = req.params.id;
  sql.query("Select * from users where id = ? ", id, function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send(result);
    }
    });
});

const createUserSchema = Joi.object({
  first_name: Joi.string().required(),
  last_name: Joi.string().required(),
  gender: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(3),
});

// POST /users
router.post("/", validator.body(createUserSchema), (req, res, next) => {
  sql.query("INSERT into users SET ?", req.body, (error, result) => {
    if (error) {
      if (error.code === "ER_DUP_ENTRY") {
        res.status(400).json({
          message: "User already exists. Please sign in or use another email.",
        });
      }
      next(error);
      return;
    }
    res.json(result);
  });
});

//Delete user by id
router.delete('/:id',function(req,res){
  //finding Index
  sql.query('DELETE FROM users WHERE id=?', [req.params.id], function(error, results, fields) {
    if (error) throw error;
    res.end('Record has been deleted!');
    });
});

//edit user by id 
router.put('/:id',function(req,res){
  console.log(req.params);
  sql.query('UPDATE users SET first_name=?, last_name=?, gender=?, email=?, password=? where id=?', [req.body.first_name, req.body.last_name, req.body.gender, req.body.email, req.body.password, req.params.id], function(error, results, fields) {
  if (error) throw error;
     res.json(results);
});
});
module.exports = router;
