var express = require('express');
var router = express.Router();
const sql = require('../db');
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({})

/* GET books listing. */
router.get('/', function(req, res, next) {
  sql.query("select * from books",function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});

/* GET books count. */
router.get('/count', function(req, res, next) {
  sql.query("select COUNT(id) as 'count' from books",function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});

// - get api to get one book by id
router.get('/:id([0-9]+)', function(req, res, next) {
  const id = req.params.id;
  sql.query("Select * from books where id = ? ", id, function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send(result);
    }
    });
});


// - get api to get one book by user_id
router.get('/user/:id([0-9]+)', function(req, res, next) {
  const id = req.params.id;
  sql.query("Select b.id, b.title, b.description, CONCAT_WS(' ', a.first_name, a.last_name) AS author from books as b join authors as a ON b.author_id = a.id where b.user_id = ? ", id, function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send(result);
    }
    });
});

// - get api to get one book by author_id
router.get('/author/:id([0-9]+)', function(req, res, next) {
  const id = req.params.id;
  sql.query("Select b.id, b.title, b.description, CONCAT_WS(' ', a.first_name, a.last_name) AS author from books as b join authors as a ON b.author_id = a.id where b.author_id = ? ", id, function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send(result);
    }
    });
});

const createBookSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  year: Joi.number().required(),
  publisher: Joi.string().required(),
  isbn: Joi.string().min(3).required(),
  author_id: Joi.number().required(),
  user_id: Joi.number().required(),
  is_available: Joi.boolean().required()
});

// POST /books
router.post("/", validator.body(createBookSchema), (req, res, next) => {
  sql.query("INSERT into books SET ?", req.body, (error, result) => {
    if (error) {
      next(error);
      return;
    }
    res.json(result);
  });
});

//Delete book by id
router.delete('/:id',function(req,res){
  //finding Index
  sql.query('DELETE FROM books WHERE id=?', [req.params.id], function(error, results, fields) {
    if (error) throw error;
    res.end('Record has been deleted!');
    });
});

//edit book by id 
router.put('/:id',function(req,res){
  sql.query('UPDATE books SET title=?, description=?, year=?, publisher=?, isbn=?, author_id=? where id=?', [req.body.title, req.body.description, req.body.year, req.body.publisher, req.body.isbn, req.body.author_id, req.params.id], function(error, results, fields) {
  if (error) throw error;
     res.json(results);
});
});

//borrow book by id 
router.patch('/borrow/:id',function(req,res){
  console.log(req.params);
  sql.query('UPDATE books SET user_id=? where id=?', [req.body.user_id, req.params.id], function(error, results, fields) {
  if (error) throw error;
     res.json(results);
});
});

//return book by id 
router.patch('/:id',function(req,res){
  console.log(req.params);
  sql.query('UPDATE books SET user_id=null, is_available=true where id=?', [req.params.id], function(error, results, fields) {
  if (error) throw error;
     res.json(results);
});
});

module.exports = router;
