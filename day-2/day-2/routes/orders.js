const express = require('express');
const router = express.Router();

// Assignment
// Create api for orders 

const orders = [
  {
    id: 1,
    productName: "Reebok shoes",
    price: 200,
    color: "black",
    quantity: 200
  },
  {
    id: 2,
    productName: "sports t shirt",
    price: 100,
    color: "blue",
    quantity: 100
  }
];

// single resource

// - get api to get one order by id
router.get('/:id', function(req, res, next) {
  const order = orders.find(o => o.id == req.params.id);
  if(order){
    res.send(order);
  }else {
    res.json({resp: "order not found"});
  }
});

// - put api to edit one order by id
router.put('/:id', function(req,res){
  console.log(req)
  const index = orders.findIndex(o => o.id == req.params.id);
  if(index >= 0){
    console.log(index);
    console.log(req.body);
    orders[index] = req.body;
  res.json({orders: orders });
  }else {
    res.json({resp: "order not found"});
  }
})

// - delete api to delete one order by id
router.delete('/:id', function(req,res){
  const index = orders.findIndex(o => o.id == req.params.id);
  if(index >= 0){
    orders.splice(index,1);
  res.json({orders: orders });
  }else {
    res.json({resp: "order not found"});
  }
})

// - post api to create a new order
router.post('/', function(req,res){
  orders.push(req.body);
  res.json({orders:orders })
});

// All resources
// - get api to fetch all the orders
router.get('/', function(req, res, next) {
  res.send({orders: orders});
});

// - put api to edit all the orders
router.put('/', function(req,res){
  orders.length = 0;
  req.body.orders.forEach(order => {
    orders.push(order);
  });
  res.json({orders: orders })
})

// - delete api to delete all orders 
router.delete('/', function(req,res){
  orders.length = 0;
  res.json({orders: orders })
})

module.exports = router;
