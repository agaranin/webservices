const express = require('express');
const router = express.Router();

let users = [
  {name:"superman", age: 777},
  {name:"catwoman", age: 35},
  {name:"batman", age: 40}
]

// single resource

  // - get api to get one user by name
  router.get('/:name', function(req, res, next) {
    const user = users.find(u => u.name == req.params.name);
    if(user){
      res.send(user);
    }else {
      res.json({resp: "user not found"});
    }
  });

  // - put api to edit one user by name
  router.put('/:name', function(req,res){
    const index = users.findIndex(u => u.name == req.params.name);
    if(index >= 0){
      users[index] = req.body;
    res.json({users: users });
    }else {
      res.json({resp: "user not found"});
    }
  })

  // - delete apit to delete one user by name
  router.delete('/:name', function(req,res){
    //const pos = users.indexOf(users.find(u => u.name == req.params.name));
    const pos = users.findIndex(u => u.name == req.params.name);
    if(pos >= 0){
    users.splice(pos,1);
    res.json({users: users });
    }else {
      res.json({resp: "user not found"});
    }
  })

  // - post api to create a new user
  router.post('/', function(req,res){
    users.push(req.body);
    res.json({users:users })
  });
  
  // All resources

  // - get api to fetch all the users
  router.get('/', function(req, res, next) {
    res.send({users:users});
  });

  // - put api to edit all the users
  router.put('/', function(req,res){
    users.length = 0;
    req.body.users.forEach(user => {
      users.push(user);
    });
    res.json({users: users })
  })

/* -- Version 2
  router.put('/', function(req,res){
    users = req.body.users;
    res.json({users: users })
  })
*/

  // - delete api to delete all users 
  router.delete('/', function(req,res){
    users.length = 0;
    // users = [];
    res.json({users: users })
  })

  /* -- Version 2
  router.delete('/', function(req,res){
    users = [];
    res.json({users: users })
  })
*/
module.exports = router;
