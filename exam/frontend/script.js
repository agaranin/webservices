const DOMAIN = "http://localhost:3000";

$(document).ready(function () {
  getAuthors();
});

function BookDetailsById(id) {
  $.ajax({
    url: `${DOMAIN}/books/user/${id}`,
    method: "GET",
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done(function (books) {
    $("#user-books").show();
    var result =
      "<table class='table table-striped table-hover table-bordered'>" + "<tr><th>ID</th>" + "<th>Title</th>" + "<th>Subtitle</th>"+ "<th>Description</th>"+ "<th>Publisher</th>"+ "<th>ISBN</th>";
    books.forEach((book) => {
      result += "<tr><td>" + book.id + "</td>";
      result += "<td>" + book.title + "</td>";
      result += "<td>" + book.subtitle + "</td>";
      result += "<td>" + book.description + "</td>";
      result += "<td>" + book.publisher + "</td>";
      result += "<td>" + book.isbn + "</td>";
      result += "</tr>" + "\n";
    });
    result += '<button class=" btn btn-success" id="btnAddEdit" onclick="HideBooks()">Cancel</button>';
    $("#user-books").html(result);
  });
}

function HideBooks() {
  $("#user-books").hide();
}


function getAuthors() {
  $.ajax({
    method: "GET",
    url: `${DOMAIN}/authors`,
    error: function (jqxhr, status, errorThrown) {
      httpErrorHandler(jqxhr, status, errorThrown);
    },
  }).done((resp) => {
    $("#user-books").hide();
    $("#user-table").show();
    var result =
      "<tr><th>ID</th>" +
      "<th>Name</th>" +
      "<th>Email</th>" +
      "<th>writing_type</th><th></th></tr>";
    resp.forEach((user) => {
      result += "<tr><td>" + user.id + "</td>";
      result += "<td>" + user.name + "</td>";
      result += "<td>" + user.email + "</td>";
      result += "<td>" + user.writing_type + "</td>";
      result +=
        '<td><a onclick="return BookDetailsById(' +
        user.id +
        ');" href="#">Book Details</a></td>';
      result += "</tr>" + "\n";
      $("#user-table").html(result);
    });
  });
}

function httpErrorHandler(jqxhr, status, errorThrown) {
  alert("AJAX error: " + jqxhr.responseText + ", status: " + jqxhr.status);
}
