const express = require('express');
const router = express.Router();
const sql = require('../db');
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({})

/* GET authors listing. */
router.get('/', function(req, res, next) {
  sql.query("select * from authors",function(error,results,fields){
    if(error) throw error;
    // I tried adding an array of books to the author, but it didn't work.
    results.forEach(author => {
      sql.query("select * from books where author_id = ?", author.id, function(error,booksResults,fields){
        author.books = [];
        booksResults.forEach(book => {
          author.books.push(book);
        });
      });
      console.log(results);
    });
    res.json(results);
  });
});

module.exports = router;
