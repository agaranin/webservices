const express = require('express');
const router = express.Router();
const sql = require('../db');
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({})

const schema = Joi.object({
  id: Joi.number().integer().required()
});

// - get api to get one user by user_id
router.get('/user/:id([0-9]+)', validator.params(schema), function(req, res, next) {
  const id = req.params.id;
  sql.query("Select b.id, b.title, b.subtitle, b.description, b.publisher, b.isbn  from books as b join authors as a ON b.author_id = a.id where b.author_id = ? ", id, function (err, result) {
    if(err) {
      console.log("error: ", err);
      res.send(err);
    }
    else{
      res.send(result);
    }
    });
});

module.exports = router;
