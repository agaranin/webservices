-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema authorsdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema authorsdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `authorsdb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `authorsdb` ;

-- -----------------------------------------------------
-- Table `authorsdb`.`authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `authorsdb`.`authors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(200) NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  `writing_type` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`, `email`, `name`, `writing_type`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `authorsdb`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `authorsdb`.`books` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NOT NULL,
  `subtitle` VARCHAR(1000) NOT NULL,
  `description` VARCHAR(3000) NOT NULL,
  `publisher` VARCHAR(200) NOT NULL,
  `isbn` VARCHAR(200) NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `books_authors_fk`
    FOREIGN KEY (`author_id`)
    REFERENCES `authorsdb`.`authors` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `books_authors_fk_idx` ON `authorsdb`.`books` (`author_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

INSERT INTO `authorsdb`.`authors` (`id`, `email`, `name`, `writing_type`) VALUES ('1', 'shakespear@gmail.com', 'Shakespear', 'Romantic');
INSERT INTO `authorsdb`.`authors` (`id`, `email`, `name`, `writing_type`) VALUES ('2', 'charles@gmail.com', 'Charles Dickens', 'Fiction');
INSERT INTO `authorsdb`.`books` (`id`, `title`, `subtitle`, `description`, `publisher`, `isbn`, `author_id`) VALUES ('1', 'Hamlet', 'An awesome book for awesome people', 'Hamlet is melancholy, bitter, and cynical, full of hatred for his uncle\'s scheming and disgust for his mother\'s sexuality. A reflective and thoughtful young man who has studied at the University of Wittenberg, Hamlet is often indecisive and hesitant, but at other times prone to rash and impulsive acts.', 'London', '5321', '1');
INSERT INTO `authorsdb`.`books` (`id`, `title`, `subtitle`, `description`, `publisher`, `isbn`, `author_id`) VALUES ('2', 'The Tempest', 'Another awesome book by your own Shakespeare', 'The Tempest Summary. Prospero uses magic to conjure a storm and torment the survivors of a shipwreck, including the King of Naples and Prospero\'s treacherous brother, Antonio. Prospero\'s slave, Caliban, plots to rid himself of his master, but is thwarted by Prospero\'s spirit-servant Ariel.', 'London', '2345', '1');
INSERT INTO `authorsdb`.`books` (`id`, `title`, `subtitle`, `description`, `publisher`, `isbn`, `author_id`) VALUES ('3', 'The Tempest', 'An awesome book for awesome people', 'Hamlet full of hatred for his uncle\'s scheming and disgust for his mother\'s sexuality. A reflective and thoughtful young man who has studied at the University of Wittenberg, Hamlet is often indecisive and hesitant, but at other times prone to rash and impulsive acts.', 'Russian', '5321', '2');
INSERT INTO `authorsdb`.`books` (`id`, `title`, `subtitle`, `description`, `publisher`, `isbn`, `author_id`) VALUES ('4', 'Hamlet', 'Another awesome book by your own Shakespeare', 'Prospero uses magic to conjure a storm and torment the survivors of a shipwreck, including the King of Naples and Prospero\'s treacherous brother, Antonio. Prospero\'s slave, Caliban, plots to rid himself of his master, but is thwarted by Prospero\'s spirit-servant Ariel.', 'Canada', '2345', '2');